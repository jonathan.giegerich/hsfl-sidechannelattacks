# Intro


    Hello World!
    We are three students of the applied computer science at the Hochschule Flensburg with main focus
    on communication technology. Jonathan Otto Giegerich, Melf Martens and Thomas Carstensen.
    In the sixth semester of our Education we have to work on a project which we should find ourselves.
    We wanted to do something new what was never done before from us or at our university. Prof. Dr.-
    Ing. Sebastian Gajek was a professor of us in some previous semesters and suggested us to do a
    project on side-channel-analysis. Because this is a very interesting project which matches absolutely
    with our focus on communication technology and also electromagnetic compatibility, we decided to
    do so.
    The project splits in two main phases. In the first phase of the project we are going to appropriate
    the new knowledge from tutorials of the company NewAE Technology. In the second phase we are
    going to operate with the new learned knowledge and we will analyze and attack some hardware
    which we will choose during the project. The hardware which is used to appropriate the knowledge is
    also from the company NewAE Technology. It is especially for learning how to do side-channel-
    analysis and side-channel-attacks and consequently to operate with the knowledge to make
    hardware much safer.
    The Hochschule Flensburg bought the Side-Channel & Glitching Starter Pack (Level 2) which includes
    not only the ChipWhisperer-Lite-Version but also an expanded Breakout Board, different probes and
    an UFO Board which can carry the target boards. This set contains everything which is needed to
    learn how side-channel-analysis works but we will maybe need to build some special probes in
    future.
    Prof. Dr.-Ing. Sebastian Gajek is our contact for everything what has to do with IT-Security and he is
    also leading the Project. Because the project requires a very good knowledge about electrical circuits
    and electromagnetic compatibility our second contact is Prof. Dr.-Ing. Klaus-Dieter Kruse who has
    very much experience with both of these topics.
    What really motivates us to do this kind of project is the fact that we can learn something that is new
    to us. This project is not only an opportunity for us to learn something new it also opens doors to the
    working world, because this area of application is very special but it exists everywhere where
    hardware is used. The consequences of not closed gaps ranging from card fraud to stealing expensive
    cars. If these gaps won ́t be closed someone can exploit them as often as he wants and that results in
    a massive financial damage.
    At this point we want to mention that we are exclusively acting as White-Hats and we won't use the
    learned techniques to harm others. If new attacks are successful we will inform manufacturer,
    creator or developer of the hardware and we will publish the implementation of such an attack.
    This blog will be updated simultaneously to the project. This is an opportunity for us to publish our
    work and experience with side-channel-analysis and it is also an opportunity for all others who are
    interested in side-channel-analysis to track our work and experience and also to learn something
    new. We will publish the newest tests and recognitions in a blog and we would be glad about
    feedback and questions.

# License

# Dependencies