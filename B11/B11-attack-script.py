###########################################################################
#
# https://wiki.newae.com/Tutorial_B11_Breaking_RSA
#
# Date: 30.07.2017
#
###########################################################################

from chipwhisperer.common.api.CWCoreAPI import CWCoreAPI
from matplotlib.pylab import *
import matplotlib.pyplot as plt
import numpy as np


# Projekt laden
cwapi = CWCoreAPI()
cwapi.openProject(r'/home/joni/side-channel-attacks/B11-Breaking-RSA/b11.cwp') # Bitte anpassen

print "Project loaded..."

# Trace Manager aus ChipWhisperer Analyse API laden
tm = cwapi.project().traceManager()
ntraces = tm.numTraces()

#Reference trace
trace_ref = tm.getTrace(0)

print "Traces loaded..."

# Reference Trace anzeigen
#plt.plot(trace_ref)
#plt.show()

# Traces Auswaehlen
# Im Tracemanager der GUI sind die 4 Keys dargestellt

# Traces : Key
#    0/1 = 80 00
#    2/3 = 81 40
#    4/5 = AB E2
#    6/7 = AB E3

target_trace_number = 5 # 5 Entspricht Key AB E2

# Beginn der RSA Decryption im Trace
start = 3600
rsa_one = trace_ref[start:(start+500)]
        
diffs = []

print "Starting diff calculation..."


for i in range(0, 15499): # 23499
    
    diff = tm.getTrace(target_trace_number)[i:(i+len(rsa_one))] - rsa_one    
    diffs.append(np.sum(abs(diff)))

plt.plot(diffs)
plt.show()

recovered_key = 0x0000
bitnum = 17

# Berechnung und Ausgabe der Delays
# Je grueßer das Delay, desto wahrscheinlicher handelt es sich um eine square+multiply berechnung
# d.h. Bit im Key = '1' bei kleineren Delays nur square, also Bit im Key '0'

# Die Einstellungen fuer die beiden IF-Condition muessen von Fall zu Fall angepasst werden

print "Delays: "

last_t = -1
for t,d in enumerate(diffs):
    
    if d < 10: #10        
        bitnum -= 1
        if last_t > 0:
            delta = t-last_t
            
            print delta
            
            if delta > 1000: # 1300
                recovered_key |= 1<<bitnum
                

        last_t = t
    
# Ausgabe des Keys

print("Key = %04x"%recovered_key)
